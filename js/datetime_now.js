/**
 * @file
 * JavaScript behaviors for datetime fields.
 */

/* eslint no-extend-native: ["error", { "exceptions": ["Date"] }] */
/* eslint func-names: off */
/* eslint jquery/no-val: off */

(function ($, Drupal) {
  Date.prototype.datetimeNowDateInput = function () {
    const local = new Date(this);
    local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
    return local.toJSON().slice(0, 10);
  };

  Date.prototype.datetimeNowTimeInput = function (step = 1) {
    const local = new Date(this);
    local.setMinutes(this.getMinutes() - this.getTimezoneOffset());

    const time = local.toJSON();

    // If step is a multiple of 60, do not return seconds.
    if (step % 60 === 0) {
      return time.slice(11, 16);
    }
    return time.slice(11, 19);
  };

  /**
   * Update a date field to today.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.datetime_now_button = {
    attach(context) {
      $(once('datetime-now', '.datetime-now-wrapper', context)).each(
        function () {
          const $wrapper = $(this);
          const $nowButton = $wrapper.find('.datetime-now');

          $nowButton.on('click', function () {
            const date = `${this.getAttribute('data-date-selector')}-date`;
            const time = `${this.getAttribute('data-date-selector')}-time`;
            const $dateElement = $wrapper.find(
              `[data-drupal-selector="${date}"]`,
            );
            const $timeElement = $wrapper.find(
              `[data-drupal-selector="${time}"]`,
            );

            const step = $timeElement.attr('step');
            const today = new Date().datetimeNowDateInput();
            const now = new Date().datetimeNowTimeInput(step);

            $dateElement.val(today);
            $timeElement.val(now);

            return false;
          });
        },
      );
    },
  };
})(jQuery, Drupal);
