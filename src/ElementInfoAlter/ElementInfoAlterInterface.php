<?php

namespace Drupal\datetime_now\ElementInfoAlter;

/**
 * Interface for element info alter handlers.
 */
interface ElementInfoAlterInterface {

  /**
   * Alter the element info array.
   *
   * @param array $info
   *   An array of element info.
   */
  public function alter(array &$info);

}
