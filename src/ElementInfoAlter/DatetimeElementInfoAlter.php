<?php

namespace Drupal\datetime_now\ElementInfoAlter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Custom form widget renderer for all datetime widgets.
 */
class DatetimeElementInfoAlter implements ElementInfoAlterInterface {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function alter(array &$info) {
    $info['datetime']['#process'][] = [$this, 'process'];
  }

  /**
   * Process callback for datetime elements.
   *
   * Adds a "now" button to fill the element with the current date and time.
   */
  public function process(&$element, FormStateInterface $form_state, &$complete_form) {
    $element['#attributes']['class'][] = 'datetime-now-wrapper';
    $element['now'] = [
      '#type'  => 'button',
      '#value' => $this->t('Now'),
      '#attributes' => [
        'class' => ['datetime-now'],
        'data-date-selector' => $element['date']['#attributes']['data-drupal-selector'] ?? '',
      ],
      '#attached' => [
        'library' => [
          'datetime_now/datetime_now',
        ],
      ],
    ];

    return $element;
  }

}
